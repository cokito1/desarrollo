/**
 * @file sockets.h
 * @author Carlos Javier González Cortés
 * @date 06 Feb 2017
 * @brief contiene las cabeceras de las funciones que nos permiten
 * abrir, enlazar, escuchar, aceptar, recibir, enviar y cerrar un socket.
 */

/**
* @page sockets Manual de la libreria sockets\n
*
* @section constants_sockets Constantes
* @subsection sub_constants_message_sockets Constantes de sockets.c
* MAX_SIZE_MESSAGE\tTamanio de mensaje maximo\n
*
* @section includedfun_sockets Funciones incluidas
* @subsection sub_socket Funciones de mantenimiento de sockets (sockets.c)
* int abrir_socketTCP()\n
* int enlazar_puerto_socket(int sockval, int num_port)\n
* int escuchar_socket(int sockval)\n
* int aceptar_conexion(int sockval)\n
* int conectar_socket(int sockfd, void* host, int port)\n
* int cerrar_socket(int sockfd)\n
*
* @authors Carlos Javier González Cortés
* @version 1.0
* @date 2017
*/
#ifndef SOCKETS_H
#define SOCKETS_H
#define MAX_SIZE_MESSAGE 512

/**
* @page abrir_socketTCP Abrir\n
*
* @section desc_abrir_socketTCP DESCRIPTION
* @subsection inc_abrir_socketTCP Header
*  #include "sockets.h"
* @subsection pro_abrir_socketTCP Prototype
*  int abrir_socketTCP()\n
*
* @subsection fun_abrir_socketTCP Function
*  Esta funcion se encarga de abrir el socket TCP.
*
*
* @return
* Devuelve 0 si la ejecucion es correcta y -1 si no
* @authors Carlos Javier González Cortés
* @version 1.0
* @date 2017
*/
int abrir_socketTCP();

/**
* @page enlazar_puerto_socket Abrir\n
*
* @section desc_enlazar_puerto_socket DESCRIPTION
* @subsection inc_enlazar_puerto_socket Header
*  #include "sockets.h"
* @subsection pro_enlazar_puerto_socket Prototype
*  int enlazar_puerto_socket(int sockval, int num_port)\n
*
* @subsection fun_enlazar_puerto_socket Function
*  Enlaza un socket a un puerto mediante la función binding().
*
*
* @return
* Devuelve 0 si la ejecucion es correcta y -1 si no
* @authors Carlos Javier González Cortés
* @version 1.0
* @date 2017
*/
int enlazar_puerto_socket(int sockval, int num_port);

/**
* @page escuchar_socket Abrir\n
*
* @section desc_escuchar_socket DESCRIPTION
* @subsection inc_escuchar_socket Header
*  #include "sockets.h"
* @subsection pro_escuchar_socket Prototype
*  int escuchar_socket(int sockval)\n
*
* @subsection fun_escuchar_socket Function
*  Se mantiene a la espera de recibir una conexión de un cliente.
*
*
* @return
* Devuelve 0 si la ejecucion es correcta y -1 si no
* @authors Carlos Javier González Cortés
* @version 1.0
* @date 2017
*/
int escuchar_socket(int sockval);

/**
* @page aceptar_conexion Abrir\n
*
* @section desc_aceptar_conexion DESCRIPTION
* @subsection inc_aceptar_conexion Header
*  #include "sockets.h"
* @subsection pro_aceptar_conexion Prototype
*  int aceptar_conexion(int sockval)\n
*
* @subsection fun_aceptar_conexion Function
*  Acepta la petición de un cliente de conectarse.
*
*
* @return
* Devuelve 0 si la ejecucion es correcta y -1 si no
* @authors Carlos Javier González Cortés
* @version 1.0
* @date 2017
*/
int aceptar_conexion(int sockval);

/**
* @page conectar_socket Abrir\n
*
* @section desc_conectar_socket DESCRIPTION
* @subsection inc_conectar_socket Header
*  #include "sockets.h"
* @subsection pro_conectar_socket Prototype
*  int conectar_socket(int sockfd, void* host, int port)\n
*
* @subsection fun_conectar_socket Function
*  Conecta un socket.
*
*
* @return
* Devuelve 0 si la ejecucion es correcta y -1 si no
* @authors Carlos Javier González Cortés
* @version 1.0
* @date 2017
*/
int conectar_socket(int sockfd, void* host, int port);

/**
* @page cerrar_socket Abrir\n
*
* @section desc_cerrar_socket DESCRIPTION
* @subsection inc_cerrar_socket Header
*  #include "sockets.h"
* @subsection pro_cerrar_socket Prototype
*  int cerrar_socket(int sockfd)\n
*
* @subsection fun_cerrar_socket Function
*  Cierra la conexión con el cliente.
*
*
* @return
* Devuelve 0 si la ejecucion es correcta y -1 si no
* @authors Carlos Javier González Cortés
* @version 1.0
* @date 2017
*/
int cerrar_socket(int sockfd);

#endif