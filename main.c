/**
 * @file main.c
 * @author Carlos Javier González Cortés
 * @date 24 Feb 2018
 * @brief función principal.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include "sockets.h"


int main(int argc, char *argv[])
{
    int puerto;
    char* host;
    size_t tam;
    int sockfd;
    struct hostent *server;

    /************** INTRODUCCIÓN DE PARÁMETROS ****************/
    if (argc < 2){
    	printf("Error en la introducción de parámetros. Ej: ./alar 8.8.8.8");
    	return -1;
    }

    tam = strlen(argv[1]);
	host = malloc(tam + 1);
	if (host == NULL)
	{
		printf("Error con el parámetro introducido.\n");
		return -1;
	}
	strcpy(host, argv[1]);

	/************************ SOCKETS *************************/

    sockfd = abrir_socketTCP();

	server = gethostbyname(host);
	
    if (server == NULL) {
        fprintf(stderr,"Error, no existe ese host.\n");
        return -1;
    }
    for(puerto = 0; puerto < 65536; puerto++){


	    if (conectar_socket(sockfd, server, puerto) < 0){
	    }
	    else{
	    	printf("Puerto %d abierto.\n", puerto);
	    }
    }
    

    cerrar_socket(sockfd);
    free(host);
    return 0;
}