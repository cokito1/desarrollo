/**
 * @file sockets.c
 * @author Carlos Javier González Cortés
 * @date 06 Feb 2018
 * @brief contiene las funciones que nos permiten
 * abrir, enlazar, escuchar, aceptar, recibir, enviar y cerrar un socket.
 */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <sys/syslog.h>
#include <string.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <time.h>

#define MAX_CONNECTIONS	100
#define h_addr h_addr_list[0]
int abrir_socketTCP(){
	int sockval;
	syslog (LOG_INFO, "Creando socket");

	if ((sockval = socket(AF_INET, SOCK_STREAM, 0)) < 0 ){
		syslog(LOG_ERR, "ERROR: Error creando el socket: %s", strerror(errno));
		return -1;
	}

	return sockval;
}

int enlazar_puerto_socket(int sockval, int num_port){
	struct sockaddr_in Direccion;


    bzero(&Direccion, sizeof(struct sockaddr)); 
	Direccion.sin_family = AF_INET; /* TCP/IP family */
	Direccion.sin_port = htons(num_port); /* Asigning port */
	Direccion.sin_addr.s_addr = htonl(INADDR_ANY); /* Accept all adresses */

	syslog (LOG_INFO, "Binding socket");

	if (bind (sockval, (struct sockaddr *)&Direccion, sizeof(Direccion)) < 0){
		syslog(LOG_ERR, "ERROR: Error binding socket: %s", strerror(errno));
		close(sockval);
		return -1;
	}

	return 0;
}

int escuchar_socket(int sockval){
	syslog (LOG_INFO, "Listening connections");
	if (listen (sockval, MAX_CONNECTIONS) < 0){
		syslog(LOG_ERR, "Error listenining");
		return -1;
	}
	return 0;
}

int aceptar_conexion(int sockval){
	int desc;
	int tam;
	struct sockaddr* Direccion = NULL;
	desc = accept(sockval, Direccion, (unsigned int*)&tam);
	if (desc < 0){
		syslog(LOG_ERR, "Error accepting connection");
		return -1;
	}

	return desc;
}

int conectar_socket(int sockfd, void *host, int port){
	struct sockaddr_in saddr;
	struct hostent* aux = NULL;

	aux = (struct hostent *) host;

	bzero(&saddr, sizeof(saddr));

	saddr.sin_family = AF_INET; 
	saddr.sin_port = htons(port); 
	saddr.sin_addr = *((struct in_addr *)aux->h_addr);

	return connect(sockfd,(struct sockaddr *)&saddr, sizeof(saddr));
}

int cerrar_socket(int sockfd){
	syslog(LOG_INFO, "Socket cerrado.");
	return close(sockfd);
}