#-------------------------------------
# Autor: Carlos Javier González Cortés
#-------------------------------------

CC = gcc -g
CFLAGS = -Wall -ansi -pedantic

all: alar

.PHONY : clean
clean:
	rm -f *.o alar

afterclean :
	@rm -f *.o

alar: main.o sockets.o
	@echo "#------------------------------"
	@echo "# Generando $@ "
	@echo "# Depende de $^"
	@echo "# Ha cambiado $<"
	$(CC) $(CFLAGS) -o $@ main.o sockets.o -rdynamic -pthread

%.o : %.c %.h
	@echo "#------------------------------"
	@echo "# Generando $@ "
	@echo "# Depende de $^"
	@echo "# Ha cambiado $<"
	$(CC) $(CFLAGS) -c $<
